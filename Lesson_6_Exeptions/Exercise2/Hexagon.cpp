#include "Hexagon.h"



Hexagon::Hexagon(std::string nam, std::string col, double newSide) : Shape(col, nam)
{
	this->side = newSide;
}

double Hexagon::getSide()
{
	return this->side;
}

void Hexagon::setSide(double newSide)
{
	if (newSide <= 0)
	{
		throw shapeException();
	}
	this->side = newSide;
}

double Hexagon::CalArea()
{
	return MathUtlis().CalHexagonArea(this->side);
}

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "size is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;
}


