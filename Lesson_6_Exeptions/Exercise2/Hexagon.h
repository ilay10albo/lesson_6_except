#pragma once
#include "shape.h"
#include "shapeException.h"
class Hexagon :
	public Shape
{
private:
	double side;
public:
	Hexagon(std::string nam, std::string col, double newSide);
	double getSide(); // Get side size
	void setSide(double newSide); // Set side size
	double CalArea(); // Get area of hexagon
	void draw(); // Print details of hexagone


};

