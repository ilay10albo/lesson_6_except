#include "Pentagon.h"


Pentagon::Pentagon(std::string nam, std::string col, double newSide): Shape(col, nam)
{
	this->side = newSide;
}


double Pentagon::getSide()
{
	return this->side;
}

void Pentagon::setSide(double newSide)
{
	if (newSide <= 0)
	{
		throw shapeException();
	}
	this->side = newSide;
}

double Pentagon::CalArea()
{
	return MathUtlis().CalPentagonArea(this->side);
}

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "size is " << getSide() << std::endl << "Area: " << CalArea() << std::endl;;
}


