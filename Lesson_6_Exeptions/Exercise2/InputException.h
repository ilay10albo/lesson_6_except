#pragma once
#include <exception>
#include <iostream>
#include <string>
class InputException :  public std::exception
{
public:
	virtual const char* what() const
	{
		return "This is input exception!\n"; // string when int is expected
	}
	
};

