#include "MathUtlis.h"

double MathUtlis::CalPentagonArea(double lenOfSide)
{
	double area;
	area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * lenOfSide * lenOfSide) / 4;
	return area;
}

double MathUtlis::CalHexagonArea(double lenOfSide)
{
	double area;
	area = (6 * (lenOfSide*lenOfSide)) / (4 * tan(3.1415926 / 6));
	return area;
}

