#include <iostream>
using namespace std;

int add(int a, int b) {
	if (a == 8200 || b == 8200 || a + b == 8200)
	{
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get"));
	}
  return a + b;
}

int  multiply(int a, int b) {
	add(a, b);
	int sum = 0;
  for(int i = 0; i < b; i++) {
    sum = add(sum, a);
  };
  if (sum == 8200)
  {
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get"));

  }
  return sum;
}

int  pow(int a, int b) {
	if (a == 8200|| b == 8200)
	{
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get"));
	}
  int exponent = 1;
  for(int i = 0; i < b; i++) {
    exponent = multiply(exponent, a);
  };
  if (8200 == exponent)
  {
	  throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get"));
  }
  return exponent;
}

int main(void) {
	try
	{
		cout << multiply(8200, 1) << endl;
		cout << add(8199, 1) << endl;
		cout << pow(11, 5) << endl;
	}
	catch (const std::string& errorString)
	{
		cout << "ERROR " << errorString.c_str();
	}getchar();
}