#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "letterException.h"
#include "MathUtlis.h"

#include "Pentagon.h"
#include "Hexagon.h"

#include <string.h>
int main()
{
	std::cout << MathUtlis().CalPentagonArea(5);
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0;
	double side = 1;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	
	// === Added Shapes === // 
	Pentagon pent(col, nam, side);
	Hexagon hexa(col, nam, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpent = &pent; // Added ... 
	Shape *ptrhex = &hexa; // Added ...
	
	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; 
	char shapetype = 'a';
	char x = 'y';
	std::string testInputString = "";
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, z = pentagon, e = hexagon\n" << std::endl;
		std::cin >> testInputString; // Getting input as string
		x = testInputString[0];
		if (std::cin.fail())
		{
			throw InputException();
		}
		try
		{
			if (testInputString.size() > 1 || testInputString.size() == 0)
			{
				throw letterException();
			}
			shapetype = testInputString[0];

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				
				if (std::cin.fail())
				{
					throw InputException();
				}
				
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;

				if (std::cin.fail())
				{
					throw InputException();
				}
				
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				if (std::cin.fail())
				{
					throw InputException();
				}
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				if (std::cin.fail())
				{
					throw InputException();
				}
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'z':
				std::cout << "enter name, color, and side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					throw InputException();
				}
				pent.setName(nam);
				pent.setColor(col);
				pent.setSide(side);
				ptrpent->draw();
				break;
			case 'e':
				std::cout << "enter name, color, and side" << std::endl;
				std::cin >> nam >> col >> side;
				if (std::cin.fail())
				{
					throw InputException();
				}
				hexa.setName(nam);
				hexa.setColor(col);
				hexa.setSide(side);
				ptrhex->draw();

				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl; 
			std::cin.get() >> x; getchar();
		}
		catch (InputException &e)
		{
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			printf(e.what());
		}
		catch (letterException  &err)
		{
			printf(err.what());
		}
		
		catch (const std::exception &e) // Fix to address
		{			
			printf(e.what());
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}
	system("pause");
	return 0;
	
}