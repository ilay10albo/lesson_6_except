#pragma once
#include "shape.h"
#include "shapeException.h"

class Pentagon :
	public Shape
{

public:
	Pentagon(std::string nam, std::string col , double newSide);
	double getSide(); // Get side of pentagon
	void setSide(double newSide); // Set side size of pentagon
	double CalArea(); // Get area of pentagon
	void draw(); // Print details of pentagon
private:
	double side;
};

